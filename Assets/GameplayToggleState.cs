﻿using UnityEngine;

namespace Igloo.GameCommands
{
    public class GameplayToggleState : GameCommandHandler
    {
        enum ObjectState { Active, Disabled, Broken};
        private ObjectState curObjectState = ObjectState.Disabled;

        public override void PerformInteraction()
        {
            switch (interactionType)
            {
                case GameCommandType.Activate:
                    curObjectState = ObjectState.Active;
                    break;
                case GameCommandType.Deactivate:
                    curObjectState = ObjectState.Disabled;
                    break;
                case GameCommandType.Destroy:
                    curObjectState = ObjectState.Broken;
                    break;
            }
            UpdateObjectState();
        }

        private void UpdateObjectState()
        {
            Debug.Log($"{gameObject.name} state: {curObjectState}");
        }
    }
}
