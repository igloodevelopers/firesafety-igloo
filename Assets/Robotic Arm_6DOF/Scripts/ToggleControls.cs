﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleControls : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] GameObject IK_InActive,Manual_InActive;
    CalcIKsldr IK_Script;
    UR3Control Manual_Script;
    
    void Start()
    {
        IK_Script = GetComponent<CalcIKsldr>();
        Manual_Script = GetComponent<UR3Control>();
        IK_Script.enabled = true;
        Manual_Script.enabled = false;
        IK_InActive.SetActive(false);
        Manual_InActive.SetActive(true);

    }

    // Update is called once per frame
    void Update()
    {
       
        
    }
    public void Toggle_Control(bool NewValue)
    {
        if(NewValue)
        {
            IK_Script.enabled=false;
            IK_InActive.SetActive(true);
            Manual_Script.enabled = true;
            Manual_InActive.SetActive(false);
        }
        else
        {
            IK_Script.enabled = true;
            IK_InActive.SetActive(false);
            Manual_Script.enabled = false;
            Manual_InActive.SetActive(true);
        }

    }
}
