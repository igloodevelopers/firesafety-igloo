﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UR3Control : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Joint;// Robtics arms object transfrom 

    [SerializeField] Slider[] Control_slider;
    [SerializeField] Text[] DegreeText;
    private int i;
    Slider _slider;

    private float[] previousValue = new float[6];

    // Start is called before the first frame update
    void Start()
    {
        Control_slider[0].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[0].value, 0, 360); });
        Control_slider[1].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[1].value, 1, 360); });
        Control_slider[2].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[2].value, 2, 360); });
        Control_slider[3].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[3].value, 3, 360); });
        Control_slider[4].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[4].value, 4, 360); });
        Control_slider[5].onValueChanged.AddListener(delegate { OnSliderChanged(Control_slider[5].value, 5, 360); });
        previousValue[0] = Control_slider[0].value;
        previousValue[1] = Control_slider[1].value;
        previousValue[2] = Control_slider[2].value;
        previousValue[3] = Control_slider[3].value;
        previousValue[4] = Control_slider[4].value;
        previousValue[5] = Control_slider[5].value;

    }

    // Update is called once per frame
    void Update()
    {
        DegreeText[0].text = Control_slider[0].value.ToString("F1") + "°";
        DegreeText[1].text = Control_slider[1].value.ToString("F1") + "°";
        DegreeText[2].text = Control_slider[2].value.ToString("F1") + "°";
        DegreeText[3].text = Control_slider[3].value.ToString("F1") + "°";
        DegreeText[4].text = Control_slider[4].value.ToString("F1") + "°";
        DegreeText[5].text = Control_slider[5].value.ToString("F1") + "°";
    }
    void OnSliderChanged(float value, int i, float rotationvalue)
    {
        float delta = value - this.previousValue[i];
        if (i == 0 || i == 4)
            Joint[i].transform.Rotate(Vector3.up * delta);
        else
            Joint[i].transform.Rotate(Vector3.back * delta);
        this.previousValue[i] = value;
    }
    public void IncrementButton(int SliderIndex)
    {
        _slider = GameObject.Find("ContolSlider"+SliderIndex.ToString()).GetComponent<Slider>();
            if(_slider.value >= _slider.minValue&&_slider.value <= _slider.maxValue)
            {
                _slider.value += 1f;
                Debug.Log(_slider.value);
            }
    }
    public void DecrementButton(int SliderIndex)
    {
        _slider = GameObject.Find("ContolSlider" + SliderIndex.ToString()).GetComponent<Slider>();
        if (_slider.value >= _slider.minValue && _slider.value <= _slider.maxValue)
        {
            _slider.value -= 1f;
            Debug.Log(_slider.value);
        }
    }
}