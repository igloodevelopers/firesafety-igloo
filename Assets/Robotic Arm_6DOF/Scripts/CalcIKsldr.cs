﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
public class CalcIKsldr : MonoBehaviour
{
    public Transform[] Joints;

    public Slider S_Slider;    //PositionX min=-350 max=-150
    public Slider L_Slider;    //PositionY min=-50 max=0
    public Slider U_Slider;    //PositionZ min=-19 max=50
    public Slider R_Slider;    //RotationX min=-6 max=65
    public Slider B_Slider;    //RotationY min=0 max=360
    public Slider T_Slider;    //Open + Close 0.05 -> 0.6 (and the same for other side but minus)
    public static double[] theta = new double[6];    //angle of the joints
    Slider _slider, _slider2;

    private float L1, L2, L3, L4, L5, L6;    //arm length in order from base
    private float C3;

    public float px = 0f, py = 0f, pz = 0f;
    public float rx = 0f, ry = 0f, rz = 0f;

    // Use this for initialization
    void Start()
    {
        theta[0] = theta[1] = theta[2] = theta[3] = theta[4] = theta[5] = 0.0;
        L1 = 4f;
        L2 = 6f;
        L3 = 3f;
        L4 = 4f;
        L5 = 2f;
        L6 = 1f;
        C3 = 0.0f;

        px = S_Slider.value;
        py = L_Slider.value;
        pz = U_Slider.value;
        rx = R_Slider.value;
        ry = B_Slider.value;
        rz = T_Slider.value;
    }

    // Update is called once per frame
    void Update()
    {
        float ax, ay, az, bx, by, bz;
        float asx, asy, asz, bsx, bsy, bsz;
        float p5x, p5y, p5z;
        float C1, C23, S1, S23;

        px = S_Slider.value;
        py = L_Slider.value;
        pz = U_Slider.value;
        rx = R_Slider.value;
        ry = B_Slider.value;
        rz = T_Slider.value;

        ax = Mathf.Cos(rz * 3.14f / 180.0f) * Mathf.Cos(ry * 3.14f / 180.0f);
        ay = Mathf.Sin(rz * 3.14f / 180.0f) * Mathf.Cos(ry * 3.14f / 180.0f);
        az = -Mathf.Sin(ry * 3.14f / 180.0f);

        p5x = px - (L5 + L6) * ax;
        p5y = py - (L5 + L6) * ay;
        p5z = pz - (L5 + L6) * az;

        theta[0] = Mathf.Atan2(p5y, p5x);

        C3 = (Mathf.Pow(p5x, 2) + Mathf.Pow(p5y, 2) + Mathf.Pow(p5z - L1, 2) - Mathf.Pow(L2, 2) - Mathf.Pow(L3 + L4, 2)) / (2 * L2 * (L3 + L4));
        theta[2] = Mathf.Atan2(Mathf.Pow(1 - Mathf.Pow(C3, 2), 0.5f), C3);

        float M = L2 + (L3 + L4) * C3;
        float N = (L3 + L4) * Mathf.Sin((float)theta[2]);
        float A = Mathf.Pow(p5x * p5x + p5y * p5y, 0.5f);
        float B = p5z - L1;
        theta[1] = Mathf.Atan2(M * A - N * B, N * A + M * B);

        C1 = Mathf.Cos((float)theta[0]);
        C23 = Mathf.Cos((float)theta[1] + (float)theta[2]);
        S1 = Mathf.Sin((float)theta[0]);
        S23 = Mathf.Sin((float)theta[1] + (float)theta[2]);

        bx = Mathf.Cos(rx * 3.14f / 180.0f) * Mathf.Sin(ry * 3.14f / 180.0f) * Mathf.Cos(rz * 3.14f / 180.0f) - Mathf.Sin(rx * 3.14f / 180.0f) * Mathf.Sin(rz * 3.14f / 180.0f);
        by = Mathf.Cos(rx * 3.14f / 180.0f) * Mathf.Sin(ry * 3.14f / 180.0f) * Mathf.Sin(rz * 3.14f / 180.0f) - Mathf.Sin(rx * 3.14f / 180.0f) * Mathf.Cos(rz * 3.14f / 180.0f);
        bz = Mathf.Cos(rx * 3.14f / 180.0f) * Mathf.Cos(ry * 3.14f / 180.0f);

        asx = C23 * (C1 * ax + S1 * ay) - S23 * az;
        asy = -S1 * ax + C1 * ay;
        asz = S23 * (C1 * ax + S1 * ay) + C23 * az;
        bsx = C23 * (C1 * bx + S1 * by) - S23 * bz;
        bsy = -S1 * bx + C1 * by;
        bsz = S23 * (C1 * bx + S1 * by) + C23 * bz;

        theta[3] = Mathf.Atan2(asy, asx);
        theta[4] = Mathf.Atan2(Mathf.Cos((float)theta[3]) * asx + Mathf.Sin((float)theta[3]) * asy, asz);
        theta[5] = Mathf.Atan2(Mathf.Cos((float)theta[3]) * bsy - Mathf.Sin((float)theta[3]) * bsx, -bsz / Mathf.Sin((float)theta[4]));

        Joints[0].transform.localEulerAngles = new Vector3(0, 0, (float)theta[0] * Mathf.Rad2Deg);
        //Joints[0].transform.localEulerAngles = new Vector3(0, 0, px);

        Joints[1].transform.localEulerAngles = new Vector3(0, (float)theta[1] * Mathf.Rad2Deg, 0);

        Joints[2].transform.localEulerAngles = new Vector3(0, (float)theta[2] * Mathf.Rad2Deg, 0);

        Joints[3].transform.localEulerAngles = new Vector3(0, (float)theta[3] * Mathf.Rad2Deg, 0);
        Joints[4].transform.localEulerAngles = new Vector3(0, 0, (float)theta[4] * Mathf.Rad2Deg);

        Joints[6].transform.localPosition = new Vector3(rz, 0, -0.402f);
        Joints[5].transform.localPosition = new Vector3(-rz, 0, -0.402f);
    }
    public void IK_Inc_Button(string Axis)
    {
        _slider = GameObject.Find("P" + Axis).GetComponent<Slider>();
        if (_slider.value >= _slider.minValue && _slider.value <= _slider.maxValue)
        {
            _slider.value += 0.1f;
            Debug.Log(_slider.value);
        }

    }
    public void HeadRotate_Inc(string Axis)
    {
        _slider2 = GameObject.Find("R" + Axis).GetComponent<Slider>();
        if (_slider2.value >= _slider2.minValue && _slider2.value <= _slider2.maxValue)
        {
            _slider2.value += 1f;
            Debug.Log(_slider2.value);
        }

    }

    public void IK_Dec_Button(string Axis)
    {
        _slider = GameObject.Find("P" + Axis).GetComponent<Slider>();
        if (_slider.value >= _slider.minValue && _slider.value <= _slider.maxValue)
        {
            _slider.value -= 0.1f;
            Debug.Log(_slider.value);
        }
    }

    public void HeadRotate_Dec(string Axis)
    {
        _slider2 = GameObject.Find("R" + Axis).GetComponent<Slider>();
        if (_slider2.value >= _slider2.minValue && _slider2.value <= _slider2.maxValue)
        {
            _slider2.value -= 1f;
            Debug.Log(_slider2.value);
        }
    }
}
