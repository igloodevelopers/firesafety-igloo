﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ToolHeadPosition : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Text[] ToolHeadText;
    GameObject ToolHead;
    GameObject Base;
    void Start()
    {
        ToolHead = GameObject.Find("Head");
        Base = GameObject.Find("Base");
    }

    // Update is called once per frame
    void Update()
    {
        ToolHeadText[0].text = Mathf.Abs(ToolHead.transform.position.x - Base.transform.position.x).ToString("F3");
        ToolHeadText[1].text = Mathf.Abs(ToolHead.transform.position.y - Base.transform.position.y).ToString("F3");
        ToolHeadText[2].text = Mathf.Abs(ToolHead.transform.position.z - Base.transform.position.z).ToString("F3");
        ToolHeadText[3].text = Mathf.Abs(ToolHead.transform.rotation.x - Base.transform.rotation.x).ToString("F3");
        ToolHeadText[4].text = Mathf.Abs(ToolHead.transform.rotation.y - Base.transform.rotation.y).ToString("F3");
        ToolHeadText[5].text = Mathf.Abs(ToolHead.transform.rotation.z - Base.transform.rotation.z).ToString("F3");


    }
}
