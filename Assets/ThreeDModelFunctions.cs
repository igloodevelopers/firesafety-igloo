﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

[Serializable]
public class SubMeshes
{
    public MeshRenderer meshRenderer;
    public Vector3 originalPosition;
    public Vector3 explodedPosition;
}

public class ThreeDModelFunctions : MonoBehaviour
{
    #region Variables
    public List<SubMeshes> childMeshRenderers;
    bool isInExplodedView = false;
    public float explosionSpeed = 0.1f;
    bool isMoving = false;
    private Igloo.PlayerManager playerManager;
    private bool hasTriggeredRecently = false;
    private float spread = 3f;
    #endregion

    #region UnityFunctions
    private void Awake()
    {
        childMeshRenderers = new List<SubMeshes>();
        foreach (var item in GetComponentsInChildren<MeshRenderer>())
        {
            SubMeshes mesh = new SubMeshes
            {
                meshRenderer = item,
                originalPosition = item.transform.position
            };
            mesh.explodedPosition = UnityEngine.Random.insideUnitSphere * spread;
            childMeshRenderers.Add(mesh);
        }
    }

    private void Update()
    {
        if (!playerManager)
        {
            playerManager = FindObjectOfType<Igloo.PlayerManager>();
        }
        else
        {
            if (playerManager.VRControllerTriggerButton && !hasTriggeredRecently)
            {
                ToggleExplodedView();
                hasTriggeredRecently = true;
            }
            else if(Input.GetButtonDown("Submit"))
            {
                ToggleExplodedView();
            }
        }


        if (isMoving)
        {
            if (isInExplodedView)
            {
                foreach (var item in childMeshRenderers)
                {
                    item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.explodedPosition, explosionSpeed);
                    if (Vector3.Distance(item.meshRenderer.transform.position, item.explodedPosition) < 0.001f)
                    {
                        isMoving = false;
                    }
                }
            }
            else
            {
                foreach (var item in childMeshRenderers)
                {
                    item.meshRenderer.transform.position = Vector3.Lerp(item.meshRenderer.transform.position, item.originalPosition, explosionSpeed);
                    if (Vector3.Distance(item.meshRenderer.transform.position, item.originalPosition) < 0.001f)
                    {
                        isMoving = false;
                    }
                }
            }
        }
    }
    #endregion

    #region CustomFunctions
    public void SetNewSpeed(float speed)
    {
        explosionSpeed = speed;
    }

    public void SetNewSpread(float newSpread)
    {
        spread = newSpread;
        foreach (var item in childMeshRenderers)
        {
            item.explodedPosition = UnityEngine.Random.insideUnitSphere * newSpread;
        }
    }

    public void ToggleExplodedView()
    {
        if (isInExplodedView)
        {
            isInExplodedView = false;
            isMoving = true;
        }
        else
        {
            isInExplodedView = true;
            isMoving = true;
        }
    }

    IEnumerator WaitBetweenClicks()
    {
        yield return new WaitForSeconds(0.5f);
        hasTriggeredRecently = false;
    }
    #endregion

}