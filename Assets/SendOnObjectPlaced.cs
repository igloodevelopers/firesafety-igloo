﻿using UnityEngine;

namespace Igloo.GameCommands
{
    public class SendOnObjectPlaced : TriggerCommand
    {
        public LayerMask layers;
        private SphereCollider sphere = null;
        private int thisScore = 0;

        [Header("Placement Zone sizes")]
        public float[] placementZoneSizes;
        public GameObject[] excludedObjects;
        private GameObject collisionObject;
        private bool bObjectIsInZone = false;

        private void Start()
        {
            sphere = this.gameObject.AddComponent<SphereCollider>();
            sphere.isTrigger = true;
            sphere.radius = placementZoneSizes[placementZoneSizes.Length - 1];
        }

        private void OnTriggerEnter(Collider other)
        {
            if (0 != (layers.value & 1 << other.gameObject.layer))
            {
                foreach(GameObject obj in excludedObjects)
                {
                    if (obj.name == other.gameObject.name)
                        return;
                }
                // it's not on the excluded objects list, so we can use it. 
                collisionObject = other.gameObject;
                bObjectIsInZone = true;
                Invoke("CheckColliderDistance", 10.0f);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == layers.value)
            {
                CancelInvoke("CheckColliderDistance");
                bObjectIsInZone = false;
                collisionObject = null;
            }
        }

        private void CheckColliderDistance()
        {
            if (!bObjectIsInZone) return;
            float distance = Vector3.Distance(transform.position, collisionObject.transform.position);
            for (int i = 0; i < placementZoneSizes.Length; i++)
            {
                if(distance <= placementZoneSizes[i])
                {
                    thisScore++;
                }
            }
            GameplayScoreSystem.Score += thisScore;
            missedMessage = "";
            Send();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            for(int i = 0; i < placementZoneSizes.Length; i++)
            {
                Gizmos.DrawWireSphere(transform.position, placementZoneSizes[i]);
            }
        }
    }
}
