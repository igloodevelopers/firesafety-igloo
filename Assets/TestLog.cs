﻿using System.Collections;
using UnityEngine;

public class TestLog : MonoBehaviour
{
    string testLog;
    Queue testLogQueue = new Queue();

    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    void HandleLog(string logString, string stackTrace, LogType type)
    {
        testLog = logString;
        string newString = "\n [" + type + "] : " + testLog;
        testLogQueue.Enqueue(newString);
        if (type == LogType.Exception)
        {
            newString = "\n" + stackTrace;
            testLogQueue.Enqueue(newString);
        }
        testLog = string.Empty;
        foreach (string mylog in testLogQueue)
        {
            testLog += mylog;
        }
    }

    void OnGUI()
    {
        GUILayout.Label(testLog);
    }
}
