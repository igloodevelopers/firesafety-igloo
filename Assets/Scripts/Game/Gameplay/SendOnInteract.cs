using UnityEngine;

namespace Igloo.GameCommands
{
    [RequireComponent(typeof(InteractionSystem.Interactable))]
    public class SendOnInteract : TriggerCommand
    {
        public bool bShouldAddToScore = true;
        private bool bHasRunOnce = false;
        protected override void IsSelected(bool isSelected)
        {
            if (oneShot)
            {
                if (bHasRunOnce)
                {
                    return;
                }
                var interactionSys = GetComponent<InteractionSystem.Interactable>();
                interactionSys.enabled = false;
                interactionSys.highlightOnHover = false;
            }
            bHasRunOnce = true;
            Send();
            missedMessage = "";
            if (bShouldAddToScore) { GameplayScoreSystem.Score++; }
        }
    }
}