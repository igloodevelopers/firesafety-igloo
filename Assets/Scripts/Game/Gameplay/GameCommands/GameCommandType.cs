﻿namespace Igloo.GameCommands
{
    public enum GameCommandType
    {
        None,
        Activate,
        Deactivate,
        Open,
        Close,
        Spawn,
        Destroy,
        Start,
        Stop
    }
}
