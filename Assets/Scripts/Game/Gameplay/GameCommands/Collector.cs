﻿using System.Collections.Generic;
using UnityEngine;

namespace Igloo.GameCommands
{
    public class Collector : MonoBehaviour
    {
        public bool attachCollectables = false;

        Dictionary<string, int> collections = new Dictionary<string, int>();
        public virtual void OnCollect(Collectable collectable)
        {
            if (attachCollectables)
                collectable.transform.parent = transform;
            if (collections.TryGetValue(collectable.name, out int count))
                collections[collectable.name] = count + 1;
            else
                collections[collectable.name] = 1;
        }

        public bool HasCollectable(string name)
        {
            return collections.ContainsKey(name);
        }

        public bool HasCollectableQuantity(string name, int requiredCount)
        {
            if (collections.TryGetValue(name, out int count))
                return count >= requiredCount;
            return false;
        }
    }


}
