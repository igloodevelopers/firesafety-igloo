﻿using System.Collections.Generic;
using UnityEngine;

namespace Igloo.GameCommands
{
    //Class used to call the proper GameCommandHandler subclass to a given GameCommandType received from a subclass of SendGameCommand
    public class GameCommandReceiver : MonoBehaviour
    {
        Dictionary<GameCommandType, List<System.Action>> handlers = new Dictionary<GameCommandType, List<System.Action>>();

        public void Receive(GameCommandType e)
        {
            if (handlers.TryGetValue(e, out List<System.Action> callbacks))
            {
                foreach (var i in callbacks) i();
            }
        }

        public void Register(GameCommandType type, GameCommandHandler handler)
        {
            if (!handlers.TryGetValue(type, out List<System.Action> callbacks))
            {
                callbacks = handlers[type] = new List<System.Action>();
            }
            callbacks.Add(handler.OnInteraction);
        }

        public void Remove(GameCommandType type, GameCommandHandler handler)
        {
            handlers[type].Remove(handler.OnInteraction);
        }


    }
}
