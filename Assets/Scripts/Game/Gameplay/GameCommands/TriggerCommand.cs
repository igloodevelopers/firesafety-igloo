﻿using UnityEngine;

namespace Igloo.GameCommands
{
    public abstract class TriggerCommand : SendGameCommand
    {
        public string missedMessage = "";

        protected override void Reset()
        {
            if (LayerMask.LayerToName(gameObject.layer) == "Default")
                gameObject.layer = LayerMask.NameToLayer("Environment");
            var c = GetComponent<Collider>();
            if (c != null)
                c.isTrigger = true;

            base.Reset();
        }

        protected override void IsSelected(bool isSelected)
        {
            base.IsSelected(isSelected);
        }
    }
}
