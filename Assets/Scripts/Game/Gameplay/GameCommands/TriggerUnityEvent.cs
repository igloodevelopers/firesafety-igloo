﻿using UnityEngine.Events;

namespace Igloo.GameCommands
{

    public class TriggerUnityEvent : GameCommandHandler
    {
        public UnityEvent unityEvent;

        public override void PerformInteraction()
        {
            unityEvent.Invoke();
        }
    }
}
