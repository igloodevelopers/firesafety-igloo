﻿using UnityEngine;

public class LookAtPlayer : MonoBehaviour
{
    [HideInInspector] Transform playerT = null;

    private void Update()
    {
        if (playerT)
        {
            this.transform.LookAt(playerT);
        }  
        else
        {
            if (Camera.main)
            {
                playerT = Camera.main.transform;
            }
            else return;
        }
    }
}
