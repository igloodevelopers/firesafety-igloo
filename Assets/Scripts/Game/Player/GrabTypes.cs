﻿namespace Igloo.InteractionSystem
{
    public enum GrabTypes
    {
        None,
        Trigger,
        Pinch,
        Grip,
        Scripted,
    }
}