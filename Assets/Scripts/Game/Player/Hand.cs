﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Events;


namespace Igloo.InteractionSystem
{
    public class Hand : MonoBehaviour
    {
        // The flags used to determine how an object is attached to the hand.
        [Flags]
        public enum AttachmentFlags
        {
            SnapOnAttach = 1 << 0, // The object should snap to the position of the specified attachment point on the hand.
            DetachOthers = 1 << 1, // Other objects attached to this hand will be detached.
            DetachFromOtherHand = 1 << 2, // This object will be detached from the other hand.
            ParentToHand = 1 << 3, // The object will be parented to the hand.
            VelocityMovement = 1 << 4, // The object will attempt to move to match the position and rotation of the hand.
            TurnOnKinematic = 1 << 5, // The object will not respond to external physics.
            TurnOffGravity = 1 << 6, // The object will not respond to external physics.
            AllowSidegrade = 1 << 7, // The object is able to switch from a pinch grab to a grip grab. Decreases likelyhood of a good throw but also decreases likelyhood of accidental drop
        };

        public const AttachmentFlags defaultAttachmentFlags = AttachmentFlags.ParentToHand |
                                                      AttachmentFlags.DetachOthers |
                                                      AttachmentFlags.DetachFromOtherHand |
                                                      AttachmentFlags.TurnOnKinematic |
                                                      AttachmentFlags.SnapOnAttach |
                                                      AttachmentFlags.TurnOffGravity;

        public PoseBehaviour trackedObject;
        public LayerMask CrosshairLayers;

        public bool useHoverSphere = true;
        public Transform hoverSphereTransform;
        public float hoverSphereRadius = 0.05f;
        public LayerMask hoverLayerMask = -1;
        public float hoverUpdateInterval = 0.1f;

        [Tooltip("A transform on the hand to center attached objects on")]
        public Transform objectAttachmentPoint;
        [Tooltip("A transform to detatch objects to when they are not held")]
        public Transform objectWorldParent;

        [Tooltip("A transform for the direction to look at, parented alongside the hand")]
        public Transform objectToDirectRay;

        public Camera cam;
        public float MaxDistanceNoItem = 10.0f;
        public float MaxDistanceWithItem = 0.5f;
        private float InteractorDistance = -1.0f;

        public struct AttachedObject
        {
            public GameObject attachedObject;
            public Interactable interactable;
            public Rigidbody attachedRigidbody;
            public CollisionDetectionMode collisionDetectionMode;
            public bool attachedRigidbodyWasKinematic;
            public bool attachedRigidbodyUsedGravity;
            public GameObject originalParent;
            public bool isParentedToHand;
            public GrabTypes grabbedWithType;
            public AttachmentFlags attachmentFlags;
            public Vector3 initialPositionalOffset;
            public Quaternion initialRotationalOffset;
            public Transform attachedOffsetTransform;
            public Transform handAttachmentPointTransform;
            public Vector3 easeSourcePosition;
            public Quaternion easeSourceRotation;
            public float attachTime;

            public bool HasAttachFlag(AttachmentFlags flag)
            {
                return (attachmentFlags & flag) == flag;
            }
        }

        private List<AttachedObject> attachedObjects = new List<AttachedObject>();

        public ReadOnlyCollection<AttachedObject> AttachedObjects
        {
            get { return attachedObjects.AsReadOnly(); }
        }

        public bool HoverLocked { get; private set; }
        private TextMesh debugText;
        private int prevOverlappingColliders = 0;

        private const int ColliderArraySize = 32;
        private Collider[] overlappingColliders;

        private Interactable _hoveringInteractable;

        public bool IsActive
        {
            get
            {
                if (trackedObject != null)
                    return trackedObject.isActive;

                return this.gameObject.activeInHierarchy;
            }
        }

        public bool IsPoseValid
        {
            get
            {
                return trackedObject.isValid;
            }
        }

        //-------------------------------------------------
        // The Interactable object this Hand is currently hovering over
        //-------------------------------------------------
        public Interactable HoveringInteractable
        {
            get { return _hoveringInteractable; }
            set
            {
                if (_hoveringInteractable != value)
                {
                    if (_hoveringInteractable != null)
                    {
                        _hoveringInteractable.SendMessage("OnHandHoverEnd", this, SendMessageOptions.DontRequireReceiver);

                        //Note: The _hoveringInteractable can change after sending the OnHandHoverEnd message so we need to check it again before broadcasting this message
                        if (_hoveringInteractable != null)
                        {
                            this.BroadcastMessage("OnParentHandHoverEnd", _hoveringInteractable, SendMessageOptions.DontRequireReceiver); // let objects attached to the hand know that a hover has ended
                        }
                    }

                    _hoveringInteractable = value;

                    if (_hoveringInteractable != null)
                    {
                        _hoveringInteractable.SendMessage("OnHandHoverBegin", this, SendMessageOptions.DontRequireReceiver);

                        //Note: The _hoveringInteractable can change after sending the OnHandHoverBegin message so we need to check it again before broadcasting this message
                        if (_hoveringInteractable != null)
                        {
                            this.BroadcastMessage("OnParentHandHoverBegin", _hoveringInteractable, SendMessageOptions.DontRequireReceiver); // let objects attached to the hand know that a hover has begun
                        }
                    }
                }
            }
        }

        //-------------------------------------------------
        // Active GameObject attached to this Hand
        //-------------------------------------------------
        public GameObject CurrentAttachedObject
        {
            get
            {
                CleanUpAttachedObjectStack();

                if (attachedObjects.Count > 0)
                {
                    return attachedObjects[attachedObjects.Count - 1].attachedObject;
                }

                return null;
            }
        }

        public AttachedObject? CurrentAttachedObjectInfo
        {
            get
            {
                CleanUpAttachedObjectStack();

                if (attachedObjects.Count > 0)
                {
                    return attachedObjects[attachedObjects.Count - 1];
                }

                return null;
            }
        }

        //-------------------------------------------------
        // Attach a GameObject to this GameObject
        //
        // objectToAttach - The GameObject to attach
        // flags - The flags to use for attaching the object
        // attachmentPoint - Name of the GameObject in the hierarchy of this Hand which should act as the attachment point for this GameObject
        //-------------------------------------------------
        public void AttachObject(GameObject objectToAttach, AttachmentFlags flags = defaultAttachmentFlags, Transform attachmentOffset = null)
        {
            AttachedObject attachedObject = new AttachedObject
            {
                attachmentFlags = defaultAttachmentFlags,
                attachedOffsetTransform = attachmentOffset,
                attachTime = Time.time
            };
            

            //Make sure top object on stack is non-null
            CleanUpAttachedObjectStack();

            //Detach the object if it is already attached so that it can get re-attached at the top of the stack
            if (ObjectIsAttached(objectToAttach))
                DetachObject(objectToAttach);

            if (CurrentAttachedObject)
            {
                CurrentAttachedObject.SendMessage("OnHandFocusLost", this, SendMessageOptions.DontRequireReceiver);
            }

            attachedObject.attachedObject = objectToAttach;
            attachedObject.interactable = objectToAttach.GetComponent<Interactable>();
            attachedObject.handAttachmentPointTransform = this.transform;

            attachedObject.originalParent = objectToAttach.transform.parent?.gameObject;

            // If the object just wants to be clicked, not attached. Just send the attached message and leave it.
            if (attachedObject.interactable.SelectOnly)
            {
                UpdateHovering();
                objectToAttach.SendMessage("OnAttachedToHand", this, SendMessageOptions.DontRequireReceiver);
                return;
            }

            attachedObject.attachedRigidbody = objectToAttach.GetComponent<Rigidbody>();
            if (attachedObject.attachedRigidbody != null)
            {
                attachedObject.attachedRigidbodyWasKinematic = attachedObject.attachedRigidbody.isKinematic;
                attachedObject.attachedRigidbodyUsedGravity = attachedObject.attachedRigidbody.useGravity;
            }

            if (attachedObject.HasAttachFlag(AttachmentFlags.ParentToHand))
            {
                //Parent the object to the hand
                objectToAttach.transform.parent = this.transform;
                attachedObject.isParentedToHand = true;
            }
            else
            {
                attachedObject.isParentedToHand = false;
            }

            if (attachedObject.HasAttachFlag(AttachmentFlags.SnapOnAttach))
            {
                    if (attachmentOffset != null)
                    {
                        //offset the object from the hand by the positional and rotational difference between the offset transform and the attached object
                        Quaternion rotDiff = Quaternion.Inverse(attachmentOffset.transform.rotation) * objectToAttach.transform.rotation;
                        objectToAttach.transform.rotation = attachedObject.handAttachmentPointTransform.rotation * rotDiff;

                        Vector3 posDiff = objectToAttach.transform.position - attachmentOffset.transform.position;
                        objectToAttach.transform.position = attachedObject.handAttachmentPointTransform.position + posDiff;
                    }
                    else
                    {
                        //snap the object to the center of the attach point
                        objectToAttach.transform.rotation = attachedObject.handAttachmentPointTransform.rotation;
                        objectToAttach.transform.position = attachedObject.handAttachmentPointTransform.position;
                    }

                    Transform followPoint = objectToAttach.transform;

                    attachedObject.initialPositionalOffset = attachedObject.handAttachmentPointTransform.InverseTransformPoint(followPoint.position);
                    attachedObject.initialRotationalOffset = Quaternion.Inverse(attachedObject.handAttachmentPointTransform.rotation) * followPoint.rotation;
                
            }
            else
            {
                    if (attachmentOffset != null)
                    {
                        //get the initial positional and rotational offsets between the hand and the offset transform
                        Quaternion rotDiff = Quaternion.Inverse(attachmentOffset.transform.rotation) * objectToAttach.transform.rotation;
                        Quaternion targetRotation = attachedObject.handAttachmentPointTransform.rotation * rotDiff;
                        Quaternion rotationPositionBy = targetRotation * Quaternion.Inverse(objectToAttach.transform.rotation);

                        Vector3 posDiff = (rotationPositionBy * objectToAttach.transform.position) - (rotationPositionBy * attachmentOffset.transform.position);

                        attachedObject.initialPositionalOffset = attachedObject.handAttachmentPointTransform.InverseTransformPoint(attachedObject.handAttachmentPointTransform.position + posDiff);
                        attachedObject.initialRotationalOffset = Quaternion.Inverse(attachedObject.handAttachmentPointTransform.rotation) * (attachedObject.handAttachmentPointTransform.rotation * rotDiff);
                    }
                    else
                    {
                        attachedObject.initialPositionalOffset = attachedObject.handAttachmentPointTransform.InverseTransformPoint(objectToAttach.transform.position);
                        attachedObject.initialRotationalOffset = Quaternion.Inverse(attachedObject.handAttachmentPointTransform.rotation) * objectToAttach.transform.rotation;
                    }
                
            }



            if (attachedObject.HasAttachFlag(AttachmentFlags.TurnOnKinematic))
            {
                if (attachedObject.attachedRigidbody != null)
                {
                    attachedObject.collisionDetectionMode = attachedObject.attachedRigidbody.collisionDetectionMode;
                    if (attachedObject.collisionDetectionMode == CollisionDetectionMode.Continuous)
                        attachedObject.attachedRigidbody.collisionDetectionMode = CollisionDetectionMode.Discrete;

                    attachedObject.attachedRigidbody.isKinematic = true;
                }
            }

            if (attachedObject.HasAttachFlag(AttachmentFlags.TurnOffGravity))
            {
                if (attachedObject.attachedRigidbody != null)
                {
                    attachedObject.attachedRigidbody.useGravity = false;
                }
            }

            attachedObjects.Add(attachedObject);

            UpdateHovering();
            objectToAttach.SendMessage("OnAttachedToHand", this, SendMessageOptions.DontRequireReceiver);
        }

        public bool ObjectIsAttached(GameObject go)
        {
            for (int attachedIndex = 0; attachedIndex < attachedObjects.Count; attachedIndex++)
            {
                if (attachedObjects[attachedIndex].attachedObject == go)
                    return true;
            }

            return false;
        }

        public void ForceHoverUnlock()
        {
            HoverLocked = false;
        }

        //-------------------------------------------------
        // Detach this GameObject from the attached object stack of this Hand
        //
        // objectToDetach - The GameObject to detach from this Hand
        //-------------------------------------------------
        public void DetachObject(GameObject objectToDetach, bool restoreOriginalParent = true)
        {
            int index = attachedObjects.FindIndex(l => l.attachedObject == objectToDetach);
            if (index != -1)
            {
                GameObject prevTopObject = CurrentAttachedObject;

                Transform parentTransform = objectWorldParent?.transform;
                if (attachedObjects[index].isParentedToHand)
                {
                    if (restoreOriginalParent && (attachedObjects[index].originalParent != null))
                    {
                        parentTransform = attachedObjects[index].originalParent.transform;
                    }

                    if (attachedObjects[index].attachedObject != null && attachedObjects[index].attachedObject.activeInHierarchy)
                    {
                        attachedObjects[index].attachedObject.transform.parent = parentTransform;
                    }
                }

                if (attachedObjects[index].HasAttachFlag(AttachmentFlags.TurnOnKinematic))
                {
                    if (attachedObjects[index].attachedRigidbody != null)
                    {
                        attachedObjects[index].attachedRigidbody.isKinematic = attachedObjects[index].attachedRigidbodyWasKinematic;
                        attachedObjects[index].attachedRigidbody.collisionDetectionMode = attachedObjects[index].collisionDetectionMode;
                    }
                }

                if (attachedObjects[index].HasAttachFlag(AttachmentFlags.TurnOffGravity))
                {
                    if (attachedObjects[index].attachedObject != null)
                    {
                        if (attachedObjects[index].attachedRigidbody != null)
                            attachedObjects[index].attachedRigidbody.useGravity = attachedObjects[index].attachedRigidbodyUsedGravity;
                    }
                }

                if (attachedObjects[index].attachedObject != null)
                {
                    if (attachedObjects[index].interactable == null || (attachedObjects[index].interactable != null && attachedObjects[index].interactable.IsDestroying == false))
                        attachedObjects[index].attachedObject.SetActive(true);

                    attachedObjects[index].attachedObject.SendMessage("OnDetachedFromHand", this, SendMessageOptions.DontRequireReceiver);
                }

                attachedObjects.RemoveAt(index);

                CleanUpAttachedObjectStack();

                GameObject newTopObject = CurrentAttachedObject;

                HoverLocked = false;


                //Give focus to the top most object on the stack if it changed
                if (newTopObject != null && newTopObject != prevTopObject)
                {
                    newTopObject.SetActive(true);
                    newTopObject.SendMessage("OnHandFocusAcquired", this, SendMessageOptions.DontRequireReceiver);
                }
            }

            CleanUpAttachedObjectStack();

        }


        //-------------------------------------------------
        // Get the world velocity of the VR Hand.
        //-------------------------------------------------
        public Vector3 GetTrackedObjectVelocity(float timeOffset = 0)
        {
            if (trackedObject == null)
            {
                GetUpdatedAttachedVelocities(CurrentAttachedObjectInfo.Value, out Vector3 velocityTarget, out Vector3 angularTarget);
                return velocityTarget;
            }

            if (IsActive)
            {
                if (timeOffset == 0)
                    return this.GetComponent<Rigidbody>().velocity;
                else
                {
                    return this.GetComponent<Rigidbody>().velocity; 
                }
            }

            return Vector3.zero;
        }


        //-------------------------------------------------
        // Get the world space angular velocity of the VR Hand.
        //-------------------------------------------------
        public Vector3 GetTrackedObjectAngularVelocity(float timeOffset = 0)
        {
            if (trackedObject == null)
            {
                GetUpdatedAttachedVelocities(CurrentAttachedObjectInfo.Value, out Vector3 velocityTarget, out Vector3 angularTarget);
                return angularTarget;
            }

            if (IsActive)
            {
                if (timeOffset == 0)
                    return this.GetComponent<Rigidbody>().angularVelocity;
                else
                {
                    return this.GetComponent<Rigidbody>().angularVelocity;
                }
            }

            return Vector3.zero;
        }

        public void GetEstimatedPeakVelocities(out Vector3 velocity, out Vector3 angularVelocity)
        {
            velocity = this.GetComponent<Rigidbody>().velocity;
            angularVelocity = this.GetComponent<Rigidbody>().angularVelocity;
        }


        //-------------------------------------------------
        private void CleanUpAttachedObjectStack()
        {
            attachedObjects.RemoveAll(l => l.attachedObject == null);
        }


        //-------------------------------------------------
        protected virtual void Awake()
        {
            if (hoverSphereTransform == null)
                hoverSphereTransform = this.transform;

            if (objectAttachmentPoint == null)
                objectAttachmentPoint = this.transform;

            if (trackedObject == null)
            {
                trackedObject = this.gameObject.GetComponent<PoseBehaviour>();

                if (trackedObject != null)
                    trackedObject.onTransformUpdatedEvent += OnTransformUpdated;
            }
        }

        protected virtual void OnDestroy()
        {
            if (trackedObject != null)
            {
                trackedObject.onTransformUpdatedEvent -= OnTransformUpdated;
            }
        }

        protected virtual void OnTransformUpdated(PoseBehaviour updatedPose, InputSources updatedSource)
        {
            HandFollowUpdate();
        }

        //-------------------------------------------------
        protected virtual IEnumerator Start()
        {

            if (this.gameObject.layer == 0)
                Debug.LogWarning("<b>[Interaction System]</b> Hand is on default layer. This puts unnecessary strain on hover checks as it is always true for hand colliders (which are then ignored).", this);
            else
                hoverLayerMask &= ~(1 << this.gameObject.layer); //ignore self for hovering

            // allocate array for colliders
            overlappingColliders = new Collider[ColliderArraySize];

            if (cam)
            {
                yield break;
            }

            while (true)
            {
                if (IsPoseValid)
                {
                    InitController();
                    break;
                }

                yield return null;
            }
        }


        //-------------------------------------------------
        protected virtual void UpdateHovering()
        {
            if ((cam == null) && (IsActive == false))
            {
                Debug.Log("Fail 1");
                return;
            }

            if (HoverLocked)
            {
                //Debug.Log("Fail 2");
                return;
            }

            float closestDistance = float.MaxValue;
            Interactable closestInteractable = null;

            if (useHoverSphere)
            {
                float scaledHoverRadius = hoverSphereRadius * Mathf.Abs(InteractionUtils.GetLossyScale(hoverSphereTransform));
                CheckHoveringForTransform(hoverSphereTransform.position, scaledHoverRadius, ref closestDistance, ref closestInteractable, Color.green);
            }

            // Hover on this one
            HoveringInteractable = closestInteractable;
        }

        protected virtual bool CheckHoveringForTransform(Vector3 hoverPosition, float hoverRadius, ref float closestDistance, ref Interactable closestInteractable, Color debugColor)
        {
            bool foundCloser = false;

            // null out old vals
            for (int i = 0; i < overlappingColliders.Length; ++i)
            {
                overlappingColliders[i] = null;
            }

            int numColliding = Physics.OverlapSphereNonAlloc(hoverPosition, hoverRadius, overlappingColliders, hoverLayerMask.value);

            if (numColliding >= ColliderArraySize)
                Debug.LogWarning("<b>[Interaction System]</b> This hand is overlapping the max number of colliders: " + ColliderArraySize + ". Some collisions may be missed. Increase ColliderArraySize on Hand.cs");

            // DebugVar
            int iActualColliderCount = 0;

            // Pick the closest hovering
            for (int colliderIndex = 0; colliderIndex < overlappingColliders.Length; colliderIndex++)
            {
                Collider collider = overlappingColliders[colliderIndex];

                if (collider == null)
                    continue;

                Interactable contacting = collider.GetComponentInParent<Interactable>();

                // Yeah, it's null, skip
                if (contacting == null)
                    continue;

                // Ignore this collider for hovering
                IgnoreHovering ignore = collider.GetComponent<IgnoreHovering>();
                if (ignore != null)
                {
                    if (ignore.onlyIgnoreHand == null || ignore.onlyIgnoreHand == this)
                    {
                        continue;
                    }
                }

                // Can't hover over the object if it's attached
                bool hoveringOverAttached = false;
                for (int attachedIndex = 0; attachedIndex < attachedObjects.Count; attachedIndex++)
                {
                    if (attachedObjects[attachedIndex].attachedObject == contacting.gameObject)
                    {
                        hoveringOverAttached = true;
                        break;
                    }
                }

                if (hoveringOverAttached)
                    continue;

                // Best candidate so far...
                float distance = Vector3.Distance(contacting.transform.position, hoverPosition);
                //float distance = Vector3.Distance(collider.bounds.center, hoverPosition);
                bool lowerPriority = false;
                if (closestInteractable != null)
                { // compare to closest interactable to check priority
                    lowerPriority = contacting.hoverPriority < closestInteractable.hoverPriority;
                }
                bool isCloser = (distance < closestDistance);
                if (isCloser && !lowerPriority)
                {
                    closestDistance = distance;
                    closestInteractable = contacting;
                    foundCloser = true;
                }
                iActualColliderCount++;
            }

            if (iActualColliderCount > 0 && iActualColliderCount != prevOverlappingColliders)
            {
                prevOverlappingColliders = iActualColliderCount;
            }

            return foundCloser;
        }


        //-------------------------------------------------
        protected virtual void UpdateCrosshair()
        {

            Ray ray = new Ray(cam.transform.position, (objectToDirectRay.position - cam.transform.position));
            //Debug.DrawRay(cam.transform.position, (objectToDirectRay.position - cam.transform.position), Color.red, 1.0f);

            if (attachedObjects.Count > 0)
            {
                // Holding down the mouse:
                // move around a fixed distance from the camera
                transform.position = ray.origin + InteractorDistance * ray.direction;
            }
            else
            {
                Vector3 oldPosition = transform.position;

                if (Physics.Raycast(ray, out RaycastHit raycastHit, MaxDistanceNoItem, CrosshairLayers))
                {
                    transform.position = raycastHit.point;

                    // Remember this distance in case we click and drag the mouse
                    InteractorDistance = Mathf.Min(MaxDistanceNoItem, raycastHit.distance);
                }
                else if (InteractorDistance > 0.0f)
                {
                    // Move it around at the distance we last had a hit
                    transform.position = ray.origin + Mathf.Min(MaxDistanceNoItem, InteractorDistance) * ray.direction;
                }
                else
                {
                    // Didn't hit, just leave it where it was
                    transform.position = oldPosition;
                }

            }
        }


        //-------------------------------------------------
        protected virtual void OnEnable()
        {
            float hoverUpdateBegin = 0.0f;
            InvokeRepeating("UpdateHovering", hoverUpdateBegin, hoverUpdateInterval);
        }


        //-------------------------------------------------
        protected virtual void OnDisable()
        {
            CancelInvoke();
        }


        //-------------------------------------------------
        protected virtual void Update()
        {
            UpdateCrosshair();

            if (CurrentAttachedObject != null)
            {
                if (Input.GetButtonUp("Fire1"))
                {
                    DetachObject(CurrentAttachedObject, true);
                }
                
            }

            if (HoveringInteractable)
            {
                HoveringInteractable.SendMessage("HandHoverUpdate", this, SendMessageOptions.DontRequireReceiver);
                if (Input.GetButtonDown("Fire1"))
                {
                    AttachObject(HoveringInteractable.gameObject, AttachmentFlags.ParentToHand, null);
                }
            }
        }

        /// <summary>
        /// Returns true when the hand is currently hovering over the interactable passed in
        /// </summary>
        public bool IsStillHovering(Interactable interactable)
        {
            return HoveringInteractable == interactable;
        }

        protected virtual void HandFollowUpdate()
        {
            GameObject attachedObject = CurrentAttachedObject;
            if (attachedObject != null)
            {
                if (CurrentAttachedObjectInfo.Value.interactable != null)
                {
                    if (CurrentAttachedObjectInfo.Value.interactable.handFollowTransform)
                    {
                        Quaternion targetHandRotation;
                        Vector3 targetHandPosition;

                        Transform objectT = CurrentAttachedObjectInfo.Value.attachedObject.transform;
                        Vector3 oldItemPos = objectT.position;
                        Quaternion oldItemRot = objectT.transform.rotation;
                        objectT.position = TargetItemPosition(CurrentAttachedObjectInfo.Value);
                        objectT.rotation = TargetItemRotation(CurrentAttachedObjectInfo.Value);
                        Vector3 localSkelePos = objectT.InverseTransformPoint(transform.position);
                        Quaternion localSkeleRot = Quaternion.Inverse(objectT.rotation) * transform.rotation;
                        objectT.position = oldItemPos;
                        objectT.rotation = oldItemRot;

                        targetHandPosition = objectT.TransformPoint(localSkelePos);
                        targetHandRotation = objectT.rotation * localSkeleRot;
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (CurrentAttachedObject != null)
            {
                AttachedObject attachedInfo = CurrentAttachedObjectInfo.Value;
                if (attachedInfo.attachedObject != null)
                {
                    if (attachedInfo.HasAttachFlag(AttachmentFlags.VelocityMovement))
                    {
                        UpdateAttachedVelocity(attachedInfo);
                    }
                    else
                    {
                        if (attachedInfo.HasAttachFlag(AttachmentFlags.ParentToHand))
                        {
                            attachedInfo.attachedObject.transform.position = TargetItemPosition(attachedInfo);
                        }
                    }
                }
            }
        }

        protected const float MaxVelocityChange = 10f;
        protected const float VelocityMagic = 6000f;
        protected const float AngularVelocityMagic = 50f;
        protected const float MaxAngularVelocityChange = 20f;

        protected void UpdateAttachedVelocity(AttachedObject attachedObjectInfo)
        {
            bool success = GetUpdatedAttachedVelocities(attachedObjectInfo, out Vector3 velocityTarget, out Vector3 angularTarget);
            if (success)
            {
                float scale = InteractionUtils.GetLossyScale(CurrentAttachedObjectInfo.Value.handAttachmentPointTransform);
                float maxAngularVelocityChange = MaxAngularVelocityChange * scale;
                float maxVelocityChange = MaxVelocityChange * scale;

                attachedObjectInfo.attachedRigidbody.velocity = Vector3.MoveTowards(attachedObjectInfo.attachedRigidbody.velocity, velocityTarget, maxVelocityChange);
                attachedObjectInfo.attachedRigidbody.angularVelocity = Vector3.MoveTowards(attachedObjectInfo.attachedRigidbody.angularVelocity, angularTarget, maxAngularVelocityChange);
            }
        }

        /// <summary>
        /// Snap an attached object to its target position and rotation. Good for error correction.
        /// </summary>
        public void ResetAttachedTransform(AttachedObject attachedObject)
        {
            attachedObject.attachedObject.transform.position = TargetItemPosition(attachedObject);
        }

        protected Vector3 TargetItemPosition(AttachedObject attachedObject)
        {

            return CurrentAttachedObjectInfo.Value.handAttachmentPointTransform.TransformPoint(attachedObject.initialPositionalOffset);
        }

        protected Quaternion TargetItemRotation(AttachedObject attachedObject)
        {

            return CurrentAttachedObjectInfo.Value.handAttachmentPointTransform.rotation * attachedObject.initialRotationalOffset;

        }

        protected bool GetUpdatedAttachedVelocities(AttachedObject attachedObjectInfo, out Vector3 velocityTarget, out Vector3 angularTarget)
        {
            bool realNumbers = false;


            float velocityMagic = VelocityMagic;

            Vector3 targetItemPosition = TargetItemPosition(attachedObjectInfo);
            Vector3 positionDelta = (targetItemPosition - attachedObjectInfo.attachedRigidbody.position);
            velocityTarget = (positionDelta * velocityMagic * Time.deltaTime);

            if (float.IsNaN(velocityTarget.x) == false && float.IsInfinity(velocityTarget.x) == false)
            {
                if (cam)
                    velocityTarget /= 10; //hacky fix for fallback

                realNumbers = true;
            }
            else
                velocityTarget = Vector3.zero;


            Quaternion targetItemRotation = TargetItemRotation(attachedObjectInfo);
            Quaternion rotationDelta = targetItemRotation * Quaternion.Inverse(attachedObjectInfo.attachedObject.transform.rotation);


            rotationDelta.ToAngleAxis(out float angle, out Vector3 axis);

            if (angle > 180)
                angle -= 360;

                angularTarget = Vector3.zero;

            return realNumbers;
        }

        //-------------------------------------------------
        protected virtual void OnDrawGizmos()
        {
            if (useHoverSphere && hoverSphereTransform != null)
            {
                Gizmos.color = Color.green;
                float scaledHoverRadius = hoverSphereRadius * Mathf.Abs(InteractionUtils.GetLossyScale(hoverSphereTransform));
                Gizmos.DrawWireSphere(hoverSphereTransform.position, scaledHoverRadius / 2);
            }
        }


        //-------------------------------------------------
        // Continue to hover over this object indefinitely, whether or not the Hand moves out of its interaction trigger volume.
        //
        // interactable - The Interactable to hover over indefinitely.
        //-------------------------------------------------
        public void HoverLock(Interactable interactable)
        {
            HoverLocked = true;
            HoveringInteractable = interactable;
        }


        //-------------------------------------------------
        // Stop hovering over this object indefinitely.
        //
        // interactable - The hover-locked Interactable to stop hovering over indefinitely.
        //-------------------------------------------------
        public void HoverUnlock(Interactable interactable)
        {
            if (HoveringInteractable == interactable)
            {
                HoverLocked = false;
            }
        }


        //-------------------------------------------------
        private void InitController()
        {
            this.BroadcastMessage("OnHandInitialized", 0, SendMessageOptions.DontRequireReceiver); // let child objects know we've initialized
        }

        public int GetDeviceIndex()
        {
            return 0;
        }
    }

    [System.Serializable]
    public class HandEvent : UnityEvent<Hand> { }
}


