﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

namespace Igloo.InteractionSystem
{
    [RequireComponent(typeof(Interactable))]
    class InteractOnSelect : MonoBehaviour
    {
        public bool highlightObjectBeforeSelection = false;
        public UnityEvent OnSelected, OnDeselect;
        private Interactable thisInteractable;
        public bool useWaitAfterDeselect = true;

        private void Reset()
        {
            thisInteractable = GetComponent<Interactable>();
            if (highlightObjectBeforeSelection)
            {
                thisInteractable.CreateHighlightRenderers();
                thisInteractable.highlightOnHover = false;
            }
        }

        public virtual void IsSelected(bool isSelected)
        {
            if (isSelected)
            {
                OnSelected.Invoke();
            }
            else
            {
                Debug.Log("Is now not selected");
                if (useWaitAfterDeselect)
                    StartCoroutine(WaitAfterDeselect());
                else
                    OnDeselect.Invoke();
            }
        }

        void OnDrawGizmos()
        {
            Gizmos.DrawIcon(transform.position, "InteractionTrigger", false);
        }

        void OnDrawGizmosSelected()
        {

        }

        private IEnumerator WaitAfterDeselect()
        {
            yield return new WaitForSeconds(10f);
            OnDeselect.Invoke();
        }
    }
}
