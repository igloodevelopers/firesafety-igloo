﻿using System;
using UnityEngine.Events;

namespace Igloo.InteractionSystem
{
    [Serializable]
    public class PoseBehaviourEvent : UnityEvent<PoseBehaviour, InputSources> { }
}