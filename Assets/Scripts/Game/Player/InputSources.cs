﻿using System.ComponentModel;

namespace Igloo
{
    public enum InputSources
    {
        [Description("/unrestricted")]
        Any,

        [Description("/user/gamepad")]
        Gamepad,

        [Description("/user/keyboard")]
        Keyboard,

        [Description("/user/mouse")]
        mouse,
    }
}

namespace Igloo.InteractionSystem
{
    using Sources = InputSources;
}