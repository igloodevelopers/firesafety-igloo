﻿using UnityEngine;

namespace Igloo.InteractionSystem
{
    public class PoseBehaviour : MonoBehaviour
    {
        [Tooltip("If not set, relative to parent")]
        public Transform origin;

        /// <summary>Returns whether or not the current pose is in a valid state</summary>
        public bool isValid { get { return this.enabled; } }

        /// <summary>Returns whether or not the pose action is bound and able to be updated</summary>
        public bool isActive { get { return this.isActiveAndEnabled; } }

        /// <summary>This Unity event will fire whenever the position or rotation of this transform is updated.</summary>
        public PoseBehaviourEvent onTransformUpdated;

        /// <summary>This C# event will fire whenever the position or rotation of this transform is updated.</summary>
        public UpdateHandler onTransformUpdatedEvent;
        public delegate void UpdateHandler(PoseBehaviour fromAction, InputSources fromSource);
    }
}