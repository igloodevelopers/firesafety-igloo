﻿using System.Collections.Generic;
using UnityEngine;


namespace Igloo.InteractionSystem
{
    [RequireComponent(typeof(Rigidbody))]
    public class Interactable : MonoBehaviour
    {
        [Tooltip("If this object requires to be selected, but not picked up.")]
        public bool SelectOnly = false;

        public delegate void OnAttachedToHandDelegate(Hand hand);
        public delegate void OnDetachedFromHandDelegate(Hand hand);

        public event OnAttachedToHandDelegate onAttachedToHand;
        public event OnDetachedFromHandDelegate onDetachedFromHand;

        [Tooltip("Should the rendered hand lock on to and follow the object")]
        public bool handFollowTransform = true;

        [Tooltip("Set whether or not you want this interactible to highlight when hovering over it")]
        public bool highlightOnHover = true;
        protected MeshRenderer[] highlightRenderers;
        protected MeshRenderer[] existingRenderers;
        protected GameObject highlightHolder;
        protected static Material highlightMat;
        [Tooltip("An array of child gameObjects to not render a highlight for. Things like transparent parts, vfx, etc.")]
        public GameObject[] hideHighlight;

        [Tooltip("Higher is better")]
        public int hoverPriority = 0;

        [System.NonSerialized]
        public Hand attachedToHand;

        [System.NonSerialized]
        public List<Hand> hoveringHands = new List<Hand>();
        public Hand HoveringHand
        {
            get
            {
                if (hoveringHands.Count > 0)
                    return hoveringHands[0];
                return null;
            }
        }

        public bool IsDestroying { get; protected set; }
        public bool IsHovering { get; protected set; }
        public bool WasHovering { get; protected set; }

        protected virtual void Start()
        {
            highlightMat = (Material)Resources.Load("HoverHighlight", typeof(Material));
            if (SelectOnly)
            {
                GetComponent<Rigidbody>().useGravity = false;
                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
                CreateHighlightRenderers();
                UpdateHighlightRenderers();
            }
            if (highlightMat == null)
                Debug.LogError("<b>[Interaction System]</b> Hover Highlight Material is missing. Please create a material named 'HoverHighlight' and place it in a Resources folder", this);
        }

        protected virtual bool ShouldIgnoreHighlight(Component component)
        {
            return ShouldIgnore(component.gameObject);
        }

        protected virtual bool ShouldIgnore(GameObject check)
        {
            for (int ignoreIndex = 0; ignoreIndex < hideHighlight.Length; ignoreIndex++)
            {
                if (check == hideHighlight[ignoreIndex])
                    return true;
            }

            return false;
        }

        public virtual void CreateHighlightRenderers()
        {
            highlightHolder = new GameObject("Highlighter");

            MeshFilter[] existingFilters = this.GetComponentsInChildren<MeshFilter>(true);
            existingRenderers = new MeshRenderer[existingFilters.Length];
            highlightRenderers = new MeshRenderer[existingFilters.Length];

            for (int filterIndex = 0; filterIndex < existingFilters.Length; filterIndex++)
            {
                MeshFilter existingFilter = existingFilters[filterIndex];
                MeshRenderer existingRenderer = existingFilter.GetComponent<MeshRenderer>();

                if (existingFilter == null || existingRenderer == null || ShouldIgnoreHighlight(existingFilter))
                    continue;

                GameObject newFilterHolder = new GameObject("FilterHolder");
                newFilterHolder.transform.parent = highlightHolder.transform;
                MeshFilter newFilter = newFilterHolder.AddComponent<MeshFilter>();
                newFilter.sharedMesh = existingFilter.sharedMesh;
                MeshRenderer newRenderer = newFilterHolder.AddComponent<MeshRenderer>();

                Material[] materials = new Material[existingRenderer.sharedMaterials.Length];
                for (int materialIndex = 0; materialIndex < materials.Length; materialIndex++)
                {
                    materials[materialIndex] = highlightMat;
                }
                newRenderer.sharedMaterials = materials;

                highlightRenderers[filterIndex] = newRenderer;
                existingRenderers[filterIndex] = existingRenderer;
            }
        }

        protected virtual void SendMessageToListeners(bool _isSelected)
        {
            SendMessage("IsSelected", _isSelected, SendMessageOptions.DontRequireReceiver);
        }

        protected virtual void UpdateHighlightRenderers()
        {
            if (highlightHolder == null)
                return;

            for (int rendererIndex = 0; rendererIndex < highlightRenderers.Length; rendererIndex++)
            {
                MeshRenderer existingRenderer = existingRenderers[rendererIndex];
                MeshRenderer highlightRenderer = highlightRenderers[rendererIndex];

                if (existingRenderer != null && highlightRenderer != null && attachedToHand == false)
                {
                    highlightRenderer.transform.position = existingRenderer.transform.position;
                    highlightRenderer.transform.rotation = existingRenderer.transform.rotation;
                    highlightRenderer.transform.localScale = existingRenderer.transform.lossyScale;
                    highlightRenderer.enabled = IsHovering && existingRenderer.enabled && existingRenderer.gameObject.activeInHierarchy;
                }
                else if (highlightRenderer != null)
                    highlightRenderer.enabled = false;
            }
        }

        /// <summary>
        /// Called when a Hand starts hovering over this object
        /// </summary>
        protected virtual void OnHandHoverBegin(Hand hand)
        {
            WasHovering = IsHovering;
            IsHovering = true;
            if(hand) hoveringHands.Add(hand);
            if (highlightOnHover == true && WasHovering == false)
            {
                CreateHighlightRenderers();
                UpdateHighlightRenderers();
            }
        }
        public void OnHover(bool isHovering)
        {
            if (isHovering)
            {
                OnHandHoverBegin(null);
            }
            else
            {
                OnHandHoverEnd(null);
            }
        }


        /// <summary>
        /// Called when a Hand stops hovering over this object
        /// </summary>
        protected virtual void OnHandHoverEnd(Hand hand)
        {
            WasHovering = IsHovering;

            if(hand) hoveringHands.Remove(hand);
            if (hoveringHands.Count == 0)
            {
                IsHovering = false;

                if (highlightOnHover && highlightHolder != null)
                    Destroy(highlightHolder);
            }
        }

        protected virtual void Update()
        {
            if (highlightOnHover)
            {
                UpdateHighlightRenderers();

                if (IsHovering == false && highlightHolder != null)
                    Destroy(highlightHolder);
            }
        }


        protected float blendToPoseTime = 0.1f;
        protected float releasePoseBlendTime = 0.2f;

        protected virtual void OnAttachedToHand(Hand hand)
        {
            if (SelectOnly)
            {
                SendMessageToListeners(true);
                SendMessageToListeners(false);
            }
            else if (onAttachedToHand != null)
            {
                if(hand) onAttachedToHand.Invoke(hand);
                attachedToHand = hand;
            }
            
        }

        protected virtual void OnDetachedFromHand(Hand hand)
        {
            if(onDetachedFromHand != null)
            {
                if(hand) onDetachedFromHand.Invoke(hand);
                attachedToHand = null;
            }
        }

        public void OnAttach(int objID, bool isAttached)
        {
            if (isAttached)
            {
                OnAttachedToHand(null);
            }
            else
            {
                OnDetachedFromHand(null);
            }
        }

        protected virtual void OnDestroy()
        {
            if (attachedToHand != null)
            {
                IsDestroying = true;
                attachedToHand.DetachObject(this.gameObject, false);
            }

            if (highlightHolder != null && highlightOnHover)
                Destroy(highlightHolder);

        }


        protected virtual void OnDisable()
        {
            IsDestroying = true;

            if (attachedToHand != null)
            {
                attachedToHand.ForceHoverUnlock();
            }

            if (highlightHolder != null)
                Destroy(highlightHolder);
        }
    }
}