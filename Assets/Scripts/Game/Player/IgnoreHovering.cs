﻿using UnityEngine;

namespace Igloo.InteractionSystem
{
    internal class IgnoreHovering : MonoBehaviour
    {
        [Tooltip("If Hand is not null, only ignore the specified hand")]
        public Hand onlyIgnoreHand = null;
    }
}