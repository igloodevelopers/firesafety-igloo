﻿using System.Collections.Generic;
using UnityEngine;
using Moodle;
using UnityEngine.UI;

namespace Igloo.GameCommands
{
    public class GameplayScoreSystem : GameCommandHandler
    {
        public bool bUseMoodle = true;
        public static int Score = 0;
        [HideInInspector]
        public List<string> Misses = new List<string>();
        private MoodleAPI moodleAPI = null;
        [HideInInspector]
        public string username, password;
        [Header("Final Results UI")]
        public Button SubmitScoreBtn; 
        public Text MissesTextbox;
        public Text FinalScoreTextbox; 

        [Tooltip("Send a command when target count is reacted. (optional)")]
        public SendGameCommand onGameCompleteSendCommand;
        [Tooltip("Perform an action when target count is reacted. (optional)")]
        public GameCommandHandler onGameCompletePerformAction;

        public void Start()
        {
            if (bUseMoodle && !GetComponent<Moodle.MoodleAPI>()) Debug.LogError("<b>[Gameplay Score System]</b> Moodle API component not found.");
            moodleAPI = GetComponent<MoodleAPI>();
        }

        public override void PerformInteraction()
        {
            if (onGameCompletePerformAction != null) onGameCompletePerformAction.PerformInteraction();
            if (onGameCompleteSendCommand != null) onGameCompleteSendCommand.Send();

            isTriggered = true;
            GetScore();
            Time.timeScale = 0;
        }

        public void SetUsername(string str)
        {
            username = str;
            if (password != "")
                SubmitScoreBtn.interactable = true;
            else
                SubmitScoreBtn.interactable = false;
        }

        public void SetPassword(string str)
        {
            password = str;
            if (username != "")
                SubmitScoreBtn.interactable = true;
            else
                SubmitScoreBtn.interactable = false;
        }

        public void CloseApplication()
        {
            Application.Quit();
        }

        public void RetryObjective()
        {
            Score = 0;
            isTriggered = false;
            Time.timeScale = 1;
        }

        private void GetScore()
        {
            int i = 0;
            // Set the final score text on the final page
            FinalScoreTextbox.text = $"SCORE: {Score.ToString()}";
            TriggerCommand[] objs = FindObjectsOfType<TriggerCommand>();
            foreach (TriggerCommand obj in objs)
            {
                if(obj.missedMessage != "")
                {
                    Misses.Add(obj.missedMessage);
                }
            }
            string finalMissesStr = "<b>Things you might have missed:</b> \n";
            foreach (string miss in Misses)
            {
                i++;
                Debug.Log($"Fail {i}: {miss}");
                finalMissesStr = string.Concat(finalMissesStr, $" Fail {i}: {miss} \n");
            }
            MissesTextbox.text = finalMissesStr;
        }

        public void UploadResults()
        {
            if (username == "" || password == "") Debug.LogError("No username or password to submit to Moodle");
            moodleAPI.GetToken(username, password); // Login user

            moodleAPI.OnTokenRetrieved += sender =>
            {
                IDictionary<string, string> result = new ScormDataBuilder()
                        .SetLessonStatus("completed")
                        .SetMinScore(0)
                        .SetMaxScore(100)
                        .SetRawScore(Score)
                        .SetSessionTime(new System.TimeSpan(1, 10, 25))
                        .Build();

                uint scormID = 1; // Scorm ID can be retreieved using GetScorms method.

                uint attempt = 1; // Quick hardcode attempt, this can be done better by 
                // using the GetScormAttemptCount method; 

                moodleAPI.InsertScormTracks(scormID, attempt, result); // Set a new attempt
            };

            moodleAPI.OnScormTracksInserted += (sender, trackIds) =>
            {
                // A scorm attempt was sucessfully recorded. Can close app now.
            };
        }
    }
}
