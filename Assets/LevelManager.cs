﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    public int StartSceneID = 1;

    private int currentSceneID = 0;
    public GameObject player; 

    public Vector3[] positionAdjustment;
    private Igloo.DisplayManager displayManager;
    private Igloo.PlayerManager playerManager;

    private void Start()
    {
        SceneManager.LoadSceneAsync(StartSceneID, LoadSceneMode.Additive);
        currentSceneID = StartSceneID;

        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.buildIndex == currentSceneID && playerManager != null)
        {
            playerManager.transform.localPosition = positionAdjustment[currentSceneID];
            playerManager.GetComponent<Rigidbody>().useGravity = true;
        }
        else return;

        // If on the exploision level, we want to fly
        if (arg0.buildIndex == 4)
        {
            playerManager.movementMode = Igloo.PlayerManager.MOVEMENT_MODE.FLYING;
        }
    }

    public void ChangeScene(int newSceneID)
    {
                displayManager = FindObjectOfType<Igloo.DisplayManager>();
        playerManager = FindObjectOfType<Igloo.PlayerManager>();
        SceneManager.UnloadSceneAsync(currentSceneID);
        SceneManager.LoadSceneAsync(newSceneID, LoadSceneMode.Additive);
        currentSceneID = newSceneID;
        if(playerManager != null)
        {
            playerManager.GetComponent<Rigidbody>().useGravity = false;
            playerManager.transform.localPosition = positionAdjustment[newSceneID];
        }
        if(player != null) player.transform.localPosition = positionAdjustment[newSceneID];
    }
}
